# opengym.py
"""Volume 2: Open Gym
<Name> Ethan Williams
<Class> math 437 volume 4 section 2?
<Date> september 5th
"""

import gym
import numpy as np
from gym import envs
from IPython.display import clear_output
import random
import time
import math

def find_qvalues(env,alpha=.1,gamma=.6,epsilon=.1):
    """
    Use the Q-learning algorithm to find qvalues.

    Parameters:
        env (str): environment name
        alpha (float): learning rate
        gamma (float): discount factor
        epsilon (float): maximum value

    Returns:
        q_table (ndarray nxm)
    """
    # Make environment
    env = gym.make(env)
    # Make Q-table
    q_table = np.zeros((env.observation_space.n,env.action_space.n))

    # Train
    for i in range(1,100001):
        # Reset state
        state = env.reset()

        epochs, penalties, reward, = 0,0,0
        done = False

        while not done:
            # Accept based on alpha
            if random.uniform(0,1) < epsilon:
                action = env.action_space.sample()
            else:
                action = np.argmax(q_table[state])

            # Take action
            next_state, reward, done, info = env.step(action)

            # Calculate new qvalue
            old_value = q_table[state,action]
            next_max = np.max(q_table[next_state])

            new_value = (1-alpha) * old_value + alpha * (reward + gamma * next_max)
            q_table[state, action] = new_value

            # Check if penalty is made
            if reward == -10:
                penalties += 1

            # Get next observation
            state = next_state
            epochs += 1

        # Print episode number    
        if i % 100 == 0:
            clear_output(wait=True)
            print("Episode: {i}")

    print("Training finished.")
    return q_table

# Problem 1
def random_blackjack(n):
    """
    Play a random game of Blackjack. Determine the
    percentage the player wins out of n times.

    Parameters:
        n (int): number of iterations

    Returns:
        percent (float): percentage that the player
                         wins
    """
    blac = gym.make("Blackjack-v0") #create the environment
    gamestate = False
    blac.reset() #reset just in case its been run already
    steps = n
    x = 0 
    for a in range(n): #play the game the given number of times
        while gamestate == False: #play the game till its over
            score,winval,gamestate,blank = blac.step(random.randint(0,1))
        if winval > 0: #if the game was won, add one to x
            x+=1    
        blac.reset() #reset the environment for the next play
        gamestate = False
    blac.close() # close the environment since we are completely done with it
    return x/steps #return the ratio of wins to plays (about .262 in my experience)
            
        


# Problem 2
def blackjack(n=11):
    """
    Play blackjack with naive algorithm.
    
    Parameters:
        n (int): maximum accepted player hand

    Return:
        reward (int): total reward
    """
    black = gym.make("Blackjack-v0") #create the environment
    gamestate = False
    score = black.reset() #reset just in case its been run already and assigns the first hand's value to score
    x = 0
    for a in range(10000):
        while gamestate == False: #while the game is still going
            if score[0] > n: #check the value in your hand to determine if you go or stop
                gostop = 0
            else:
                gostop = 1
            score,winval,gamestate,blank = black.step(gostop) #take the step depending on the hand you had
        score = black.reset() #reset while still getting the first hand's value
        gamestate = False #reset the game
        x+=winval #add up all the win values
    return x/10000 #return the average reward value

# Problem 3
def cartpole():
    """
    Solve CartPole-v0 by checking the velocity
    of the tip of the pole

    Return:
        time (float): time cartpole is held vertical
    """
    pole = gym.make("CartPole-v0") #create the environment to use
    
    try:
        start = pole.reset()
        position,velocity,angle,tip = start[:] #assign the starting values
        done = False
        past = time.time()
        while not done:
            pole.render()
            if (tip/2 + angle/12) < 0:#based on the tip and angle, push right or left
                start,reward,done,blank = pole.step(0) #if the tip velocity and angle are generally leaning left, go left
                position,velocity,angle,tip = start[:]
            else:
                start,reward,done,blank = pole.step(1) #if the tip velocity and angle are generally going right, go right
                position,velocity,angle,tip = start[:]
            if done:
                break
            
    finally:
        pole.close() #close the environment
        return time.time() - past #return the time it lasted
        


# Problem 4
def car():
    """
    Solve MountainCar-v0 by checking the position
    of the car.

    Return:
        time (float): time to solve environment
    """
    car = gym.make("MountainCar-v0")
    try:
        start = car.reset() #get the first restart value
        x,y = start[:] #get the x and y coordinates for the first run
        done = False
        past = time.time() #start the timer
        blank = None
        hitback = False
        while not done: #when the environment isnt finished, continue looping
            car.render()
            if blank == None: #for the first time through, always go backwards
                start,reward,done,blank = car.step(0)
                x1,y1 = start[:]
                velocityx = x1-x
                velocityy = y1-y
            elif hitback: #if it ever gets past a certain point on the back slope, only push forward
                start,reward,done,blank = car.step(2)
                x1,y1 = start[:]
            elif velocityx+velocityy < 0: #if the velocities combined are less than zero, push backwards
                start,reward,done,blank = car.step(0)
                x1,y1= start[:]
            else:
                start,reward,done,blank = car.step(2) #if the velocities are generally positive push forward
                x1,y1 = start[:]
            if x1<-1.8:
                hitback = True
            velocityx = x1-x
            velocityy = y1-y
            y=y1
            x=x1
            if done:
                break
    finally:
        car.close()
        return time.time()-past #return what they want
        

# Problem 5
def taxi(q_table):
    """
    Compare naive and q-learning algorithms.

    Parameters:
        q_table (ndarray nxm): table of qvalues

    Returns:
        naive (int): reward of naive algorithm
        q_reward (int): reward of Q-learning algorithm
    """
    uber = gym.make("Taxi-v2")
    totalrewards = 0
    totalqs = 0
    for a in range(1000): #get the results of each a thousand times
        start = uber.reset()
        done = False
        totalreward = 0
        totalq =0 
        while not done: # loop through til the environment is finished
            start,reward,done,prob = uber.step(uber.action_space.sample())#choose a random action to take
            totalreward+=reward
        start = uber.reset()
        done = False
        while not done:
            start,reward,done,prob = uber.step(np.argmax(q_table[start]))#choose the action based on the given qtable
            totalq +=reward
        totalrewards += totalreward
        totalqs += totalq
    return totalrewards/1000, totalqs/1000 #return the averages


